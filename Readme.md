# Promises in browser drill-2



## Drill Instructions
* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.
* Usage of async and await is not allowed. Solve this using Promises only.

## Api's
Users API url: https://jsonplaceholder.typicode.com/users

Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913

Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

* On browser I should be able to see the todos with the user's name prefixed with @. For example it looks like this in HTML using Checkbox:


 [  ] @Mayank Bring Mercedes Benz
 [  ] @siva Buy a BMW X7
 [  ] @Venkateshwar Buy Bugatti Chiron Sport 

* Don't implement entire To Do functionality.