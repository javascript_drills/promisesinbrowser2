function getUsersAndTodos() {
  fetch(" https://jsonplaceholder.typicode.com/users")
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return Promise.all([
        data,
        fetch("https://jsonplaceholder.typicode.com/todos"),
      ]);
    })
    .then((responseArray) => {
      return Promise.all([responseArray[0], responseArray[1].json()]);
    })
    .then((responseArray) => {
      let TodoData = responseArray[1];

      console.log(TodoData);

      let UsersData = responseArray[0];

      const users = UsersData.map((userObject) => {
        let object = {};

        object.ID = userObject.id;

        object.Name = userObject.name;

        return object;
      });

      const usersTodo = TodoData.reduce((accumulator, current) => {
        let id = current.userId;

        if (accumulator[id] === undefined) {
          let temp = [];

          temp.push(current.title);
          accumulator[id] = temp;
        } else {
          accumulator[id].push(current.title);
        }

        return accumulator;
      }, {});

      RenderTodo(usersTodo, users);
    })
    .catch((error) => {
      console.log(error.message);
    });
}

getUsersAndTodos();
